
function showProjects(){

    console.log("Montre les projets")

    document.querySelector('#titre_changeant').replaceChildren("Projets");

    document.querySelector('#bouton_portfolio2').style.backgroundColor = 'rgb(240, 130, 130)';
    document.querySelector('#bouton_portfolio2').style.color = 'black';

    document.querySelector('#bouton_portfolio1').style.backgroundColor = 'red';
    document.querySelector('#bouton_portfolio1').style.color = 'white';

    document.querySelector('#case1').style.display = 'inherit';
    document.querySelector('#titre1').replaceChildren("CV en ligne");
    document.querySelector('#case1').style.background="url('Images/jeux/slider1.png')"; 
    document.querySelector('#lien1').href ="https://qkryzzljekzele4jpr7w0g-on.drv.tw/CVEnLigne/CVLoanPerchirin/CVP.html";

    document.querySelector('#case2').style.display = 'inherit';
    document.querySelector('#titre2').replaceChildren("Restructuration Web");
    document.querySelector('#case2').style.background="url('Images/jeux/restructurationWeb.png')"; 
    document.querySelector('#lien2').href ="https://qkryzzljekzele4jpr7w0g-on.drv.tw/Projet dév. Web/Projet-restructuration-Web/";
  
    document.querySelector('#case3').style.display = 'none';

     
}

function showGames(){
    console.log("Montre les Jeux")

    document.querySelector('#titre_changeant').replaceChildren("Jeux");


    document.querySelector('#bouton_portfolio1').style.backgroundColor = 'rgb(240, 130, 130)';
    document.querySelector('#bouton_portfolio1').style.color = 'black';

    document.querySelector('#bouton_portfolio2').style.backgroundColor = 'red';
    document.querySelector('#bouton_portfolio2').style.color = 'white';

    document.querySelector('#case1').style.display = 'inherit';
    document.querySelector('#titre1').replaceChildren("David Clicker");
    document.querySelector('#case1').style.background="url('Images/jeux/DavidClicker.png')"; 
    document.querySelector('#lien1').href ="https://qkryzzljekzele4jpr7w0g-on.drv.tw/DavidClicker3/Clicker/";

    document.querySelector('#case2').style.display = 'inherit';
    document.querySelector('#titre2').replaceChildren("Jeu du nombre HTML,CSS,JS");
    document.querySelector('#case2').style.background="url('Images/jeux/lefeve.png')"; 
    document.querySelector('#lien2').href ="https://qkryzzljekzele4jpr7w0g-on.drv.tw/JeuDuNombre/JeuDuNombre2/";

    document.querySelector('#case3').style.display = 'inherit';
    document.querySelector('#titre3').replaceChildren("Jeu du nombre C");
    document.querySelector('#case3').style.background="url('Images/jeux/jeuDuNombreC.png')"; 
    document.querySelector('#lien3').href ="https://gitlab.com/te1761/test";
}
